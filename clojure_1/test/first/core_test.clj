(ns first.core-test
  (:require [clojure.test :refer :all])
  (:require [first.core :refer [generate_strings]]))

(deftest generate_strings_test
  (testing "generate_strings test"
    (testing "2 characters, 2 char long"
      (is
        (=
          (generate_strings '(a b) 2)
          '("ab" "ba"))))
    (testing "3 characters, 3 chars long"
      (is
        (=
          (generate_strings '(a b c) 3)
          '("aba" "abc" "aca" "acb" "bab" "bac" "bca" "bcb" "cab" "cac" "cba" "cbc"))))
    ))