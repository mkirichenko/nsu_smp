(ns first.core)

(defn generate_strings [input_chars n]
  (reduce
    (fn [partial_strings _]
      (remove
        (fn [string]
          (some
            (fn [doubled] (clojure.string/includes? string doubled))
            (map
              (fn [char] (str char char))
              input_chars)))
        (mapcat
          (fn [char]
            (map
              #(str char %)
              partial_strings))
          input_chars)))
    input_chars
    (range 1 n)))