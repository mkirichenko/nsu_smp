(ns third.core)

(defn integrate [function]
  (fn [x]
    (let [interval 1/100
          number_of_intervals (/ x interval)
          stepped_function (fn [index] (function (* index interval)))]
      (letfn [(stepped_function_seq [i]
                (lazy-seq (cons (stepped_function i) (stepped_function_seq (dec i)))))]
        (*
          (+
            (reduce
              +
              (take
                (dec number_of_intervals)
                (stepped_function_seq (dec number_of_intervals))))
            (/
              (+ (stepped_function 0) (stepped_function number_of_intervals))
              2))
          interval)))))

(defn -main [& args]
  (let [square (fn [x] (* x x))
        integrated_square (integrate square)]
    (println
      (time (integrated_square 3))
      (time (integrated_square 4))
      (time (integrated_square 5))
      (time (integrated_square 6)))))