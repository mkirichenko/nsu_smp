def generate_strings(input_chars, n)
  (1...n).reduce(input_chars) { |partial_strings, _|
    input_chars
        .flat_map { |char|
          partial_strings
              .map { |partial_string| partial_string + char } }
        .reject { |string|
          input_chars
              .map { |char| char + char }
              .any? { |doubled| string.include? doubled }
        }
  }
end

input_chars = gets
                  .scan(/\w/)
                  .uniq
n = gets.to_i

puts "chars = #{input_chars.to_s}"
puts "n = #{n}"

generate_strings(input_chars, n)
    .each { |s| puts s }