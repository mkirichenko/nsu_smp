class RegularMessageData
  def initialize
    @levels = Hash.new(0)
    @loggers = Hash.new(0)
    @max_offset = 0
  end

  def add_message_to_logger(logger)
    @loggers[logger] += 1
  end

  def add_message_to_level(level)
    @levels[level] += 1
  end

  def max_offset=(new_value)
    @max_offset = [@max_offset, new_value].max
  end
end

log_pattern = /^
(
#regular message
(\d{4}) #year
-
(\d{2}) #month
-
(\d{2}) #day
.*
\[\s*
(\d+) #offset in milliseconds from app start
\]
\s*
(\w+) #level
\s+-\s+
(\#?[\w.]+) #logger name
\s+-\s+
(.*) #message
)
|
#exception header
(
(Caused\s+by:\s+)? #cause prefix
([\w.]+) #exception
:\s+
(.*) #exception message
)
|
#stack trace
(
\s+at\s+
([\w.$]+) #class name
\(
([\w.]+) #file name
:
(\d+) #line in file
\)
)
$/x

regular_messages_data = {}
exceptions_data = Hash.new(0)
stack_traces_file_data = {}

File
    .read("/Users/mikhail/Library/Logs/IntelliJIdea2019.2/idea.log")
    .scan(log_pattern) { |_, year, month, _, offset, level, logger, _,
        _, _, exception, _,
        _, classname, filename, line|

      unless year.nil?
        key = year + month
        data_by_month = regular_messages_data[key]
        if data_by_month.nil?
          data_by_month = RegularMessageData.new
          regular_messages_data[key] = data_by_month
        end

        data_by_month.add_message_to_logger(logger)
        data_by_month.add_message_to_level(level)
        data_by_month.max_offset = offset.to_i
      end

      unless exception.nil?
        exceptions_data[exception] += 1
      end

      unless classname.nil?
        file_data = stack_traces_file_data[filename]
        if file_data.nil?
          file_data = Hash.new(0)
          stack_traces_file_data[filename] = file_data
        end

        file_data[line] += 1
      end
    }