require 'etc'

class ConcurrentArray
  def initialize (data)
    @number_of_threads = Etc.nprocessors
    @data = data
  end

  def map
    ConcurrentArray.new(
        execute_on_threads { |data|
          data.map { |element| yield element }
        }
    )
  end

  def any?
    execute_on_threads { |data|
      data.any? { |element| yield element }
    }
        .reduce(false) { |result, value| result || value }
  end

  def all?
    execute_on_threads { |data|
      data.any? { |element| yield element }
    }
        .reduce(true) { |result, value| result && value }
  end

  def select
    ConcurrentArray.new(
        execute_on_threads { |data|
          data.select { |element| yield element }
        }
    )
  end

  def execute_on_threads
    data = @data
    number_of_threads = @number_of_threads
    number_of_new_threads = number_of_threads - 1

    data_size = data.size
    data_part_size = data_size / number_of_threads

    new_threads = (0...number_of_new_threads).map { |i|
      part = data[(i * data_part_size)...((i + 1) * data_part_size)]
      Thread.new {
        yield part
      }
    }

    processed_last_part = yield data[(number_of_new_threads * data_part_size)...data_size]

    processed_parts = new_threads.map { |thread| thread.value }
    processed_parts << processed_last_part
    processed_parts.flatten
  end

  def to_s
    @data.to_s
  end
end