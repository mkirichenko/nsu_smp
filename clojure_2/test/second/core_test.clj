(ns second.core-test
  (:require [clojure.test :refer :all])
  (:require [second.core :refer [integrate]]))

(defn square [x] (* x x))

(deftest integrate_test
  (testing "integrate testing"
    (testing "integral of square at 0"
      (is
        (=
          ((integrate square) 0)
          0)))
    (testing "integral of square at 3.0"
      (is
        (<
          (Math/abs
            (-
              ((integrate square) 3.0)
              9.0))
          0.001)))))