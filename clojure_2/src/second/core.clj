(ns second.core)

(defn integrate [function]
  (fn [x]
    (let [number_of_intervals 100
          interval (/ x number_of_intervals)
          memoized_function (memoize function)
          stepped_function (fn [index] (memoized_function (* interval index)))]
      (*
        (+
          (reduce
            +
            (map
              stepped_function
              (range 1 number_of_intervals)))
          (/
            (+ (stepped_function 0) (stepped_function number_of_intervals))
            2))
        interval))))

(defn -main [& args]
  (let [square (fn [x] (* x x))
        integrated_square (integrate square)]
    (println
      (time (integrated_square 4))
      (time (integrated_square 8))
      (time (integrated_square 16))
      (time (integrated_square 32)))))